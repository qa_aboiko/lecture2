import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Test {
    public static void main(String[] args) {
        /*
        o Скрипт А. Логин в Админ панель
1. Открыть страницу Админ панели
2. Ввести логин, пароль и нажать кнопку Логин.
3. После входа в систему нажать на пиктограмме пользователя в верхнем правом углу и выбрать опцию «Выход.»
         */
        WebDriver driver =  initChromeDriver();
        driver.navigate().to("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement loginField = driver.findElement(By.id("email"));
        loginField.sendKeys("webinar.test@gmail.com");
        WebElement passwordField = driver.findElement(By.id("passwd"));
        passwordField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        WebElement enterButton = driver.findElement(By.name("submitLogin"));
        enterButton.click();
        try {
            Thread.sleep(1000);
            WebElement userLogo = driver.findElement(By.className("employee_name"));
            userLogo.click();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement logOutButton = driver.findElement(By.id("header_logout"));
        logOutButton.click();
        driver.quit();



        /*
        o Скрипт Б. Проверка работоспособности главного меню Админ панели
        1. Войти в Админ панель (по аналогии с предыдущим скриптом)
        2. Кликнуть на каждом видимом пункте главного меню (Dashboard, Заказы, Каталог, Клиенты...) для открытия соответствующего раздела и выполнить следующее:
        a. Вывести в консоль заголовок открытого раздела.
        b. Выполнить обновление (рефреш) страницы и проверить, что пользователь остается в том же разделе после перезагрузки страницы.
        */
        WebDriver driverSecondScript =  initChromeDriver();
        driverSecondScript.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        loginField = driverSecondScript.findElement(By.id("email"));
        loginField.sendKeys("webinar.test@gmail.com");
        passwordField = driverSecondScript.findElement(By.id("passwd"));
        passwordField.sendKeys("Xcg7299bnSmMuRLp9ITw");
        enterButton = driverSecondScript.findElement(By.name("submitLogin"));
        enterButton.click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<WebElement> mainMenuElements = driverSecondScript.findElements(By.className("maintab"));
        for (int i=0;i<mainMenuElements.size();i++) {
            mainMenuElements = driverSecondScript.findElements(By.className("maintab"));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mainMenuElements.get(i).click();
            WebElement h2Element = driverSecondScript.findElement(By.cssSelector("h2"));
            String headerPageName = h2Element.getText();
            System.out.println(headerPageName);
            driverSecondScript.navigate().refresh();
            WebElement h2ElementAfterRefreshing = driverSecondScript.findElement(By.cssSelector("h2"));
            String headerPageNameAfterRefreshing = h2ElementAfterRefreshing.getText();

            if(!headerPageName.equals(headerPageNameAfterRefreshing))
                System.out.println("Page was changed after refreshing!");
            if (i > 0)
                driverSecondScript.navigate().back();
        }
        WebElement logOutButtonSecondScript  = driverSecondScript.findElement(By.xpath("//a/span/img"));
        logOutButtonSecondScript.click();
        driverSecondScript.quit();
    }
    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", Test.class.getResource("chromedriver.exe").getPath());
        return new ChromeDriver();
    }
}
